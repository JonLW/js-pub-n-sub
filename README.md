# js-pub-n-sub

[![pipeline status](https://gitlab.com/JonLW/js-pub-n-sub/badges/master/pipeline.svg)](https://gitlab.com/JonLW/js-pub-n-sub/commits/master)
[![coverage report](https://gitlab.com/JonLW/js-pub-n-sub/badges/master/coverage.svg)](https://gitlab.com/JonLW/js-pub-n-sub/commits/master)

## Has

- No external dependencies.
- Asynchronous publishing, i.e. non-blocking, decoupled. *[Inspired by pubsub-js](https://www.npmjs.com/package/pubsub-js)*.
- Small source, but no where near as tiny as *[tiny pubsub](https://github.com/cowboy/jquery-tiny-pubsub)*, but no jQuery required either.
- For usage in node (want browser).

## Install

```bash
npm i js-pub-n-sub
```

```javascript
const pubSub = require('js-pub-n-sub');
```

## Usage

Initialise a new channel:

```js
const pubSub = require('js-pub-n-sub')();
```

Subscribe to a topic namespace with a callback function:

```js
const subscriberFn = ({name, ingredients}) =>  console.log(`Mmmm, ${name} is yummy.`);
pubSub.subscribe('food', subscriberFn);
```

...In the future, publish to that topic namespace to pass data to subscribers.

```js
pubSub.publish('food', {name: 'apple\'s'});
pubSub.publish('food', {name: 'grapes'});
```

Pass the channel:

```js
require('./bakery')(pubSub);
require('./grocers')(pubSub);
```

## Why Asynchronous Publishing?

If subscriber functions are called synchronously, and one takes a long time, all subscribers behind it in the queue will have to wait.

If the calls are asynchronous, then there is no waiting.

For Example: 

```javascript
const pubSub = require('js-pub-n-sub')();

const slowFunction = () => 
    setTimeout(
        () => console.log('slow function logged'),
        3000
    )
;

const immediateFunction = () => console.log('immediate function logged');

pubSub.subscribe('go', slowFunction);
pubSub.subscribe('go', immediateFunction);

pubSub.publish('go');
```

Output:

```text
immediate function logged
slow function logged
```

## Compatibility

Node support >= 6.

Test runner support is node >= 8.

Uses ECMA features the project uses:

- Arrow functions - [Support](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions#Browser_compatibility)
- const statement - [Support](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/const#Browser_compatibility)
- Default parameters - [Support](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Default_parameters#Browser_compatibility)
- Spread in array listerals - [Support](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax#Browser_compatibility)