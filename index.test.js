const tap = require('tap');
const sinon = require('sinon');

// helpers
const isFunction = item => typeof item === 'function';
const getPubSub = (a={}) => {
    // kill node cache of the module
    delete require.cache[require.resolve('./index.js')];
    // require the module fresh after cache purged
    return require('./index.js')(a);
}

// Test Suite Sanity Checks
tap.test('Test Suite should clean-up references to the module being tested', t => {
    t.plan(1);
    t.notEqual(getPubSub(), getPubSub(), 'Should not return same reference on each request for module');
});

tap.test('Returns a pub/sub object with 3 methods: subscribe, unsubscribe, publish', t => {
    // setup
    t.plan(7);
    const pubSub = getPubSub();
    // assert
    t.equal(Object.keys(pubSub).length, 3, 'should provide only 3 functions');
    t.ok(pubSub.subscribe, 'subscribe should exist');
    t.ok(pubSub.unsubscribe, 'unsubscribe should exist');
    t.ok(pubSub.publish, 'publish should exist');
    t.ok(isFunction( pubSub['subscribe'] ), 'subscribe should be a function');
    t.ok(isFunction( pubSub['unsubscribe'] ), 'unsubscribe should be a function');
    t.ok(isFunction( pubSub['publish'] ), 'publish should be a function');
});

tap.test('A topic subscriber should recieve data publish to the topic', t => {
    // setup
    t.plan(1);
    const pubSub = getPubSub();
    
    const topic = 'test';
    const mockData = 42;
    // assert via callback from excercise
    const subscriberFn = data => t.equal(data, mockData, 'subscriber should get data');
    // excercise
    pubSub.subscribe(topic, subscriberFn);
    pubSub.publish(topic, mockData);
});

tap.test('A subscriber should not be called after unsubscribing', t => {
    // setup
    t.plan(2);
    const pubSub = getPubSub();
    
    const topic = 'test';
    const mockData = 42;
    const spy = sinon.spy();
    // excercise
    pubSub.subscribe(topic, spy);
    pubSub.publish(topic, mockData);
    pubSub.unsubscribe(topic, spy);
    pubSub.publish(topic);
    // assert
    const assertions = () => {
        t.equal(spy.args[0][0], mockData, 'should be called before unsubscribing');
        t.equal(spy.callCount, 1, 'should not be get called after unsubscribing');
    };
    setTimeout(assertions);
})
tap.test('The correct subscribers should be removed when there are multiple topics', t => {
    // setup
    t.plan(2);
    const pubSub = getPubSub();
    
    const topicA = 'testA';
    const topicB = 'testB';
    const shouldBeRecieved = 42;
    const shouldNotBeRecieved = 999;
    const spy = sinon.spy();
    // excercise
    pubSub.subscribe(topicA, spy);
    pubSub.subscribe(topicB, spy);
    pubSub.unsubscribe(topicB, spy);
    pubSub.publish(topicA, shouldBeRecieved);
    pubSub.publish(topicB, shouldNotBeRecieved);
    // assert
    const assertions = () => {
        t.equal(spy.callCount, 1, 'subscriber should be called once');
        t.equal(spy.args[0][0], shouldBeRecieved, 'subscriber should get the expected data');
    };
    setTimeout(assertions);
});

tap.test('The correct subscribers are removed when there are multiple subs in a topic', t => {
    // setup
    t.plan(2);
    const pubSub = getPubSub();
    
    const topic = 'testA';
    const spyA = sinon.spy();
    const spyB = sinon.spy();
    // excercise
    pubSub.subscribe(topic, spyA);
    pubSub.subscribe(topic, spyB);
    pubSub.unsubscribe(topic, spyB);
    pubSub.publish(topic);
    // assert
    const assertions = () => {
        t.equal(spyA.callCount, 1, 'subscriber A should be called');
        t.equal(spyB.callCount, 0, 'subscriber B should not be called');
    };
    setTimeout(assertions);
});

tap.test('A subscriber should be non-blocking (asynchronous)', t => {
    // setup
    t.plan(2);
    const pubSub = getPubSub();
    
    const topic = 'test';
    const spy = sinon.spy();
    // excercise
    pubSub.subscribe(topic, spy);
    pubSub.publish(topic);
    // assert
    t.equal(spy.callCount, 0, 'subscriber should not be synchronous');
    const assertions = () => {
        t.equal(spy.callCount, 1, 'subscriber should be async');
    };
    setTimeout(assertions);
});

tap.test('A subscriber should be scoped to their topic', t => {
    // setup
    t.plan(2);
    const pubSub = getPubSub();
    
    const topicGood = 'test';
    const topicBad = 'notMyTopic';
    const shouldBeRecieved = 42;
    const shouldNotBeRecieved = 999;
    const spy = sinon.spy();
    // excercise
    pubSub.subscribe(topicGood, spy);
    pubSub.publish(topicGood, shouldBeRecieved);
    pubSub.publish(topicBad, shouldNotBeRecieved);
    // assert
    const assertions = () => {
        t.equal(spy.callCount, 1, 'subscriber should of only been called once');
        t.equal(spy.args[0][0], shouldBeRecieved, 'subscriber should of been given expected data from the topic');
    };
    setTimeout(assertions);
});

tap.test('Multiple subscribers should be scoped to their topic\'s', t => {
    // setup
    t.plan(4);
    const pubSub = getPubSub();
    
    const topicA = 'testA';
    const topicB = 'testB';
    const mockDataA = 42;
    const mockDataB = 24;
    const spyA = sinon.spy();
    const spyB = sinon.spy();
    // excercise
    pubSub.subscribe(topicA, spyA);
    pubSub.subscribe(topicB, spyB);
    pubSub.publish(topicA, mockDataA);
    pubSub.publish(topicB, mockDataB);
    // assert
    const assertions = () => {
        t.equal(spyA.callCount, 1, 'subscriber A called should of been called once');
        t.equal(spyA.args[0][0], mockDataA, 'subscriber A should of recieved expected data');
        t.equal(spyB.callCount, 1, 'subscriber B called should of been called once');
        t.equal(spyB.args[0][0], mockDataB, 'subscriber B should of recieved expected data');
    };
    setTimeout(assertions);
});

tap.test('Unsubscribing from a topic that does not have any subscriber\'s should do nothing', t => {
    // setup
    t.plan(1);
    const spyObj = {};
    const pubSub = getPubSub(spyObj);
    const topicA = 'testA';
    const topicB = 'testB';
    const topicDud = 'NoSubscribers';
    const noop = () => {};
    // excercise
    pubSub.subscribe(topicA, noop);
    pubSub.subscribe(topicB, noop);
    // - take clone of spyObj for comparison after fake unsubscribe
    const spyObjClone = Object.assign({},spyObj);
    pubSub.unsubscribe(topicDud, noop);
    // assert
    t.same(spyObjClone, spyObj, 'Should not present any changes');
});

tap.test('Unsubscribing from a topic where the given fn is not a subscriber should do nothing', t => {
    // setup
    t.plan(1);
    const spyObj = {};
    const pubSub = getPubSub(spyObj);
    const topic = 'testA';
    const noop = () => {};
    const noopDud = () => {};
    // excercise
    pubSub.subscribe(topic, noop);
    // - take clone of spyObj for comparison after fake unsubscribe
    const spyObjClone = Object.assign({},spyObj);
    pubSub.unsubscribe(topic, noopDud);
    // assert
    t.same(spyObjClone, spyObj, 'Should not present any changes');
});