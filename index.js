/**
 * Factory that creates and returns a pub/sub interface.
 *
 * @param {object} [topicStore] - used to host the internal subscription listings. By exposing the internals at this level, expansion possibilities are broad.
 * 
 */
module.exports = topicStore => Object.freeze({
    // #### publish
    publish: (topic, data=null) => {
        // get list of subscribers assigned to given @topic
        (topicStore[topic] || [])
            // call each subscriber fn with @data asynchronoursly
            .forEach(fn => setTimeout(() => fn(data)))
    },
    // #### subscribe
    subscribe: (topic, fn) => {
        // get list of subscribers assigned to given @topic
        const topicSubscribers = topicStore[topic] || [];
        // create new topic subscribers array
        const newTopicSubscribers = [
            // with all existing subscriber functions
            ...topicSubscribers,
            // and the new one
            fn,
        ];
        // assign back to topics store
        topicStore[topic] = newTopicSubscribers;
    },
    // #### unsubscribe
    unsubscribe: (topic, fn) => {
        const topicSubscribers = topicStore[topic];
        const fnExists = topicSubscribers && topicSubscribers.indexOf(fn) !==-1;
        // if function exists to remove
        if(fnExists){
            // get its index
            const fnIndex = topicSubscribers.indexOf(fn);
            // create new topic subscribers array
            const newTopicSubscribers = [
                // with all the subscribers before the one to remove
                ...topicSubscribers.slice(0, fnIndex),
                // and all the subscribers after the one to remove
                ...topicSubscribers.slice(fnIndex+1),
            ];
            // assign back to topics store
            topicStore[topic] = newTopicSubscribers;
        }
    },
});